"use strict";

var sensor = require('../../index')
  , Device = sensor.Device
  , async = require('async')
  , deviceA = new Device({
    address: 0x68
  })
  , deviceB = new Device({
    address: 0x69
  })

var results = []

deviceA.connect(function() {
  deviceA.runAccelerometerSelfTest(function(err, isOk, res) {
    console.log('Device A self test: ', isOk, res)
    deviceA.setAccelerometerConfig(Device.const.AFS_SEL_16, false, function(err) {
      console.log(err)

      deviceB.connect(function() {
        deviceB.runAccelerometerSelfTest(function(err, isOk, res) {
          console.log('Device B self test: ', isOk, res)
          deviceA.setAccelerometerConfig(Device.const.AFS_SEL_16, false, function(err) {
            console.log(err)
            var cnt = 0
            async.whilst(function() {
              cnt++
              return cnt < 1000
            }, function(callback) {
              var time = process.hrtime()
                , ts = new Date().getTime()
              deviceA.getAcceleration(function(err, resA) {
                var diff = process.hrtime(time)
                console.log(resA)
                time = process.hrtime()
                deviceB.getAcceleration(function(err, resB) {
                  diff = process.hrtime(time)
                  console.log(resB)

                  results.push([ts, resA, resB])

                  callback()
                })
              })
            }, function(err) {
              console.log(err)
              console.log('Finished.')
            })
          })
        })
      })
    })
  })
})

//10ms PLL settling time


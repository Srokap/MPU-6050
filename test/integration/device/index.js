"use strict";

var sensor = require('../../../index')
  , should  = require('should')
  , _ = require('lodash')
  , async = require('async')

//require('coffee-trace')

process.setMaxListeners(100)

describe('Device class', function() {

  var deviceAddress1 = 0x68
  var deviceAddress2 = 0x69

  before(function(done) {

    sensor.scanDevices(1, function(err, devices) {
      if (!_.includes(devices, deviceAddress1)) {
        throw new Error('Device 1 not connected')
      }
      if (!_.includes(devices, deviceAddress2)) {
        throw new Error('Device 2 not connected')
      }
      done()
    })

  })

  beforeEach(function(done) {
    setTimeout(done, 1000)
  })

  it.skip('should read acceleration', function(done) {

    var device = new sensor.Device()

    device.connect(function() {

      var cnt = 0

      async.whilst(
        function () {
          return cnt < 20
        },
        function (callback) {
          cnt++

          device.getAcceleration(function(err) {
            should.not.exist(err)
            //var absReading = Math.sqrt(
            //  Math.pow(res[0], 2) +
            //  Math.pow(res[1], 2) +
            //  Math.pow(res[2], 2)
            //)
            ////how big is the error from 2% crossfeed?
            //var minErr = Math.sqrt(
            //  Math.pow(Math.abs(res[0]) - 0.02 * Math.abs(res[1]) - 0.02 * Math.abs(res[2]), 2) +
            //  Math.pow(Math.abs(res[1]) - 0.02 * Math.abs(res[0]) - 0.02 * Math.abs(res[2]), 2) +
            //  Math.pow(Math.abs(res[2]) - 0.02 * Math.abs(res[1]) - 0.02 * Math.abs(res[0]), 2)
            //)
            //var maxErr = Math.sqrt(
            //  Math.pow(Math.abs(res[0]) + 0.02 * Math.abs(res[1]) + 0.02 * Math.abs(res[2]), 2) +
            //  Math.pow(Math.abs(res[1]) + 0.02 * Math.abs(res[0]) + 0.02 * Math.abs(res[2]), 2) +
            //  Math.pow(Math.abs(res[2]) + 0.02 * Math.abs(res[1]) + 0.02 * Math.abs(res[0]), 2)
            //)
            //var divider = 16384 / 8
            //console.log(res, absReading / divider, minErr / divider, maxErr / divider)
            //var hrTime = process.hrtime()
            //console.log(hrTime, hrTime[0] * 1000000 + hrTime[1] / 1000)

            callback()
          })
        },
        function (err) {
          should.not.exist(err)

          done()
        }
      )
    })
  })

  it('should get self test factors for first device', function(done) {

    var device = new sensor.Device({
      address: deviceAddress1
    })

    device.connect(function() {
      device.getSelfTestFactors(function(err, res) {
        should.not.exist(err)

        res.should.have.property('XA_TEST')
        res.should.have.property('YA_TEST')
        res.should.have.property('ZA_TEST')
        res.should.have.property('XG_TEST')
        res.should.have.property('YG_TEST')
        res.should.have.property('ZG_TEST')

        done()
      })
    })
  })

  it('should get self test factors for second device', function(done) {

    var device = new sensor.Device({
      address: deviceAddress2
    })

    device.connect(function() {
      device.getSelfTestFactors(function(err, res) {
        should.not.exist(err)

        res.should.have.property('XA_TEST')
        res.should.have.property('YA_TEST')
        res.should.have.property('ZA_TEST')
        res.should.have.property('XG_TEST')
        res.should.have.property('YG_TEST')
        res.should.have.property('ZG_TEST')

        done()
      })
    })
  })

  it('should perform full self test and pass for first device', function(done) {

    var device = new sensor.Device({
      address: deviceAddress1
    })

    device.connect(function() {
      device.runAccelerometerSelfTest(function(err, isOk, res) {

        should.not.exist(err)
        isOk.should.be.true

        res.should.have.property('XA_TEST_RES')
        res.should.have.property('YA_TEST_RES')
        res.should.have.property('ZA_TEST_RES')

        device.sleep(function() {
          done()
        })
      })
    })
  })

  it('should perform full self test and pass for second device', function(done) {

    var device = new sensor.Device({
      address: deviceAddress2
    })

    device.connect(function() {
      device.runAccelerometerSelfTest(function(err, isOk, res) {

        should.not.exist(err)
        isOk.should.be.true

        res.should.have.property('XA_TEST_RES')
        res.should.have.property('YA_TEST_RES')
        res.should.have.property('ZA_TEST_RES')

        device.sleep(function() {
          done()
        })
      })
    })
  })
})

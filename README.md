# MPU-6050 sensor library

[![Build Status](https://drone.io/bitbucket.org/Srokap/mpu-6050/status.png)](https://drone.io/bitbucket.org/Srokap/mpu-6050/latest)
[![Dependency Status](https://www.versioneye.com/user/projects/54f85e9f4f3108b7d2000127/badge.svg?style=flat)](https://www.versioneye.com/user/projects/54f85e9f4f3108b7d2000127)

## Installation

```
npm install
```

## Testing

```
npm test
```

## License

See LICENSE.txt
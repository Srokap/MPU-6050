"use strict";

module.exports = function(grunt) {

  grunt.initConfig({
    jshint: {
      files: ['Gruntfile.js', 'lib/**/*.js', 'test/**/*.js'],
      options: {
        jshintrc: true
      }
    },
    watch: {
      files: ['<%= jshint.files %>'],
      tasks: ['jshint', 'exec:test_all']
    },
    exec: {
      test_all: {
        cmd: "node node_modules/.bin/_mocha test"
      },
      test_unit: {
        cmd: "node node_modules/.bin/_mocha test/unit"
      },
      test_integration: {
        cmd: "node node_modules/.bin/_mocha test/integration"
      }
    }
  })

  grunt.loadNpmTasks('grunt-contrib-jshint')
  grunt.loadNpmTasks('grunt-contrib-watch')
  grunt.loadNpmTasks('grunt-exec')

  grunt.registerTask('default', ['jshint', 'exec:test_unit'])

}

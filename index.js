"use strict";

module.exports = process.env.COV
  ? require('./lib-cov/index.js')
  : require('./lib/index.js')

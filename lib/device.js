"use strict";

var i2c = require('i2c')
  , util = require("util")
  , events = require("events")
  , debug = require('debug')('device')
  , binary = require('binary')
  , async = require('async')

/**
 * Device object
 *
 * @param config
 * @constructor
 */
function Device(config) {
  debug('Device constructor')
  events.EventEmitter.call(this)

  this._config = config || {}
}

/**
 * List of all registers as defined in datasheet.
 *
 * @link http://invensense.com/mems/gyro/documents/RM-MPU-6000A-00v4.2.pdf
 * @type {Object}
 */
Device.reg = {
  SELF_TEST_X: 0x0D,
  SELF_TEST_Y: 0x0E,
  SELF_TEST_Z: 0x0F,
  SELF_TEST_A: 0x10,
  SMPLRT_DIV: 0x19,
  CONFIG: 0x1A,
  GYRO_CONFIG: 0x1B,
  ACCEL_CONFIG: 0x1C,
  FIFO_EN: 0x23,
  I2C_MST_CTRL: 0x24,
  I2C_SLV0_ADDR: 0x25,
  I2C_SLV0_REG: 0x26,
  I2C_SLV0_CTRL: 0x27,
  I2C_SLV1_ADDR: 0x28,
  I2C_SLV1_REG: 0x29,
  I2C_SLV1_CTRL: 0x2A,
  I2C_SLV2_ADDR: 0x2B,
  I2C_SLV2_REG: 0x2C,
  I2C_SLV2_CTRL: 0x2D,
  I2C_SLV3_ADDR: 0x2E,
  I2C_SLV3_REG: 0x2F,
  I2C_SLV3_CTRL: 0x30,
  I2C_SLV4_ADDR: 0x31,
  I2C_SLV4_REG: 0x32,
  I2C_SLV4_DO: 0x33,
  I2C_SLV4_CTRL: 0x34,
  I2C_SLV4_DI: 0x35,
  I2C_MST_STATUS: 0x36,
  INT_PIN_CFG: 0x37,
  INT_ENABLE: 0x38,
  INT_STATUS: 0x3A,
  ACCEL_XOUT_H: 0x3B,
  ACCEL_XOUT_L: 0x3C,
  ACCEL_YOUT_H: 0x3D,
  ACCEL_YOUT_L: 0x3E,
  ACCEL_ZOUT_H: 0x3F,
  ACCEL_ZOUT_L: 0x40,
  TEMP_OUT_H: 0x41,
  TEMP_OUT_L: 0x42,
  GYRO_XOUT_H: 0x43,
  GYRO_XOUT_L: 0x44,
  GYRO_YOUT_H: 0x45,
  GYRO_YOUT_L: 0x46,
  GYRO_ZOUT_H: 0x47,
  GYRO_ZOUT_L: 0x48,
  EXT_SENS_DATA_00: 0x49,
  EXT_SENS_DATA_01: 0x4A,
  EXT_SENS_DATA_02: 0x4B,
  EXT_SENS_DATA_03: 0x4C,
  EXT_SENS_DATA_04: 0x4D,
  EXT_SENS_DATA_05: 0x4E,
  EXT_SENS_DATA_06: 0x4F,
  EXT_SENS_DATA_07: 0x50,
  EXT_SENS_DATA_08: 0x51,
  EXT_SENS_DATA_09: 0x52,
  EXT_SENS_DATA_10: 0x53,
  EXT_SENS_DATA_11: 0x54,
  EXT_SENS_DATA_12: 0x55,
  EXT_SENS_DATA_13: 0x56,
  EXT_SENS_DATA_14: 0x57,
  EXT_SENS_DATA_15: 0x58,
  EXT_SENS_DATA_16: 0x59,
  EXT_SENS_DATA_17: 0x5A,
  EXT_SENS_DATA_18: 0x5B,
  EXT_SENS_DATA_19: 0x5C,
  EXT_SENS_DATA_20: 0x5D,
  EXT_SENS_DATA_21: 0x5E,
  EXT_SENS_DATA_22: 0x5F,
  EXT_SENS_DATA_23: 0x60,
  I2C_SLV0_DO: 0x63,
  I2C_SLV1_DO: 0x64,
  I2C_SLV2_DO: 0x65,
  I2C_SLV3_DO: 0x66,
  I2C_MST_DELAY_CT: 0x67,
  SIGNAL_PATH_RES: 0x68,
  USER_CTRL: 0x6A,
  PWR_MGMT_1: 0x6B,
  PWR_MGMT_2: 0x6C,
  FIFO_COUNTH: 0x72,
  FIFO_COUNTL: 0x73,
  FIFO_R_W: 0x74,
  WHO_AM_I: 0x75
}

Device.const = {
  // accelerometer full scale range [g]
  AFS_SEL_2: 0,
  AFS_SEL_4: 1,
  AFS_SEL_8: 2,
  AFS_SEL_16: 3,
  // sensitivity scale factor [LSB/g]
  AFS_SEL_2_SSF: 16384,
  AFS_SEL_4_SSF: 8192,
  AFS_SEL_8_SSF: 4096,
  AFS_SEL_16_SSF: 2048,
}

util.inherits(Device, events.EventEmitter)

/**
 * Bring sensor out of the sleep mode
 */
Device.prototype.connect = function(callback) {
  debug('Device.connect')

  var self = this

  this._connection = new i2c(this._config.address || 0x68, {
    device: this._config.busPath || '/dev/i2c-1'
  })

  this._connection.on('error', function(err) {
    debug('bmp085 I2C error:', err)
  })

  this._connection.on('data', function(data) {
    debug(data)
  })

  this.setAccelerometerConfig(Device.const.AFS_SEL_16, false, function(err) {
    if (err) {
      debug('Device.connect ACCEL_CONFIG error', err)
      return callback(err)
    }

    self.wakeUp(callback || function(){})
  })
}

Device.prototype.setAccelerometerConfig = function(accelerometerFullScaleRange, selfTestEnabled, callback) {

  var val = (accelerometerFullScaleRange << 3)

  if (selfTestEnabled) {
    val |= (7 << 5)
  }

  this._connection.writeBytes(Device.reg.ACCEL_CONFIG, [val], function(err) {
    return callback(err)
  })
}

/**
 * Get acceleration reading
 */
Device.prototype.getAcceleration = function(callback) {
  debug('Device.getAcceleration')

  var self = this

  setTimeout(function() {
    self._connection.readBytes(Device.reg.ACCEL_XOUT_H, 6, function(err, buffer) {
      if (err) {
        debug('Device.getAcceleration error', err)
        return callback(err)
      }

      var data = binary.parse(buffer)
        .word16bs('X')
        .word16bs('Y')
        .word16bs('Z')
        .vars

      callback(null, [data.X, data.Y, data.Z])
    })
  }, 2)// The accelerometer output rate is 1kHz, make sure we do not sample earlier than that

}

/**
 * Get acceleration reading
 */
Device.prototype.getSelfTestFactors = function(callback) {
  debug('Device.getSelfTestFactors')

  this._connection.readBytes(Device.reg.SELF_TEST_X, 4, function(err, buffer) {
    if (err) {
      debug('Device.getSelfTestFactors error', err)
      return callback(err)
    }

    var data = binary.parse(buffer)
      .word8bu('X')
      .word8bu('Y')
      .word8bu('Z')
      .word8bu('A')
      .vars

    var results = {
      XA_TEST: (data.X >> 5) << 2 | (data.A >> 4 & 3), // 3 higher bits & 2 bits from SELF_TEST_A
      YA_TEST: (data.Y >> 5) << 2 | (data.A >> 2 & 3), // 3 higher bits & 2 bits from SELF_TEST_A
      ZA_TEST: (data.Z >> 5) << 2 | (data.A & 3), // 3 higher bits & 2 bits from SELF_TEST_A
      XG_TEST: data.X & 31, // 5 lower bits
      YG_TEST: data.Y & 31, // 5 lower bits
      ZG_TEST: data.Z & 31  // 5 lower bits
    }

    //compute factory trim values
    if (results.XA_TEST !== 0) {
      results.FT_XA = 4096 * 0.34 * Math.pow(0.92 / 0.34, (results.XA_TEST - 1) / 30)
    } else {
      results.FT_XA = 0
    }

    if (results.YA_TEST !== 0) {
      results.FT_YA = 4096 * 0.34 * Math.pow(0.92 / 0.34, (results.YA_TEST - 1) / 30)
    } else {
      results.FT_YA = 0
    }

    if (results.ZA_TEST !== 0) {
      results.FT_ZA = 4096 * 0.34 * Math.pow(0.92 / 0.34, (results.ZA_TEST - 1) / 30)
    } else {
      results.FT_ZA = 0
    }

    return callback(null, results)
  })
}

/**
 * Perform self test and compute change from the factory trim
 */
Device.prototype.runAccelerometerSelfTest = function(callback) {
  debug('Device.runSelfTest')

  var self = this

  // "When performing accelerometer self test, the full-scale range should be set to ±8g."
  var precision = Device.const.AFS_SEL_8

  async.waterfall([
    function(cb) {
      self.setAccelerometerConfig(precision, false, function(err) {
        cb(err)
      })
    },
    function(cb) {
      self.getAcceleration(function(err, normalRow) {
        cb(err, normalRow)
      })
    },
    function(normalRow, cb) {
      self.setAccelerometerConfig(precision, true, function(err) {
        cb(err, normalRow)
      })
    },
    function(normalRow, cb) {
      self.getAcceleration(function(err, testRow) {
        cb(err, normalRow, testRow)
      })
    },
    function(normalRow, testRow, cb) {
      self.getSelfTestFactors(function(err, testFactors) {
        cb(err, normalRow, testRow, testFactors)
      })
    }
  ], function(err, normalRow, testRow, testFactors) {
    if (err) {
      debug('Device.runSelfTest error', err)
      return callback(err)
    }

    var results = {}

    results.XA_TEST_RES = ((testRow[0] - normalRow[0]) - testFactors.FT_XA) / testFactors.FT_XA
    results.YA_TEST_RES = ((testRow[1] - normalRow[1]) - testFactors.FT_YA) / testFactors.FT_YA
    results.ZA_TEST_RES = ((testRow[2] - normalRow[2]) - testFactors.FT_ZA) / testFactors.FT_ZA

    // allowed value is from -14% to 14%
    var passed = true
    if (results.XA_TEST_RES <= -0.14 || results.XA_TEST_RES >= 0.14) {
      passed = false
    }
    if (results.YA_TEST_RES <= -0.14 || results.YA_TEST_RES >= 0.14) {
      passed = false
    }
    if (results.ZA_TEST_RES <= -0.14 || results.ZA_TEST_RES >= 0.14) {
      passed = false
    }

    return callback(null, passed, results)
  })
}

/**
 * Bring sensor out of the sleep mode
 */
Device.prototype.wakeUp = function(callback) {
  debug('Device.wakeUp')

  var self = this

  this._connection.readBytes(Device.reg.PWR_MGMT_1, 1, function(err, buffer) {
    if (err) {
      debug('Device.wakeUp error', err)
      return callback(err)
    }

    self._connection.writeBytes(Device.reg.PWR_MGMT_1, [buffer[0] & ~(1 << 6)], function(err) {
      if (err) {
        debug('Device.wakeUp error', err)
        return callback(err)
      }

      // 10ms PLL settling time
      setTimeout(callback, 30)
    })
  })
}

/**
 * Put sensor into the sleep mode
 */
Device.prototype.sleep = function(callback) {
  debug('Device.sleep')

  var self = this

  this._connection.readBytes(Device.reg.PWR_MGMT_1, 1, function(err, buffer) {
    if (err) {
      debug('Device.sleep error', err)
      return callback(err)
    }

    self._connection.writeBytes(Device.reg.PWR_MGMT_1, [buffer[0] | (1 << 6)], function(err) {
      if (err) {
        debug('Device.sleep error', err)
        return callback(err)
      }

      return callback()
    })
  })
}

module.exports = Device
